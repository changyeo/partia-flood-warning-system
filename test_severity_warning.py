# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 15:43:28 2018

@author: Ivan
"""

import pytest

from floodsystem.flood import stations_level_over_threshold
from random_data import test_stations


def test():
    #get stations
    stations = test_stations()
    
        #dictionary of risk
    risk = dict(
            severe = [],
            high = [],
            moderate = [],
            low = []
            )
    
   
    
    #divides stations into groups and avoid having one station in more than 1 group
    risk['severe']  = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations,tol = 1.5)]
    risk['high'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 1.2)
                        if (i[0].name, i[1]) not in risk['severe']]
    risk['moderate'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 0.9)
                        if (i[0].name, i[1]) not in risk['severe'] and (i[0].name, i[1]) not in risk['high']]
    risk['low'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 0.5)
                        if (i[0].name, i[1]) not in risk['severe'] and (i[0].name, i[1]) not in risk['high']and (i[0].name, i[1]) not in risk['moderate']]
    
    
    
         
            
 
    
    assert risk['severe'][0][0] == 'severe_station'
    assert risk['high'][0][0] == 'high_station'
    assert risk['moderate'][0][0] == 'moderate_station'
    assert risk['low'][0][0] == 'low_station'
    
    
