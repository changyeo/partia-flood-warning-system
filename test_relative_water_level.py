import pytest

from floodsystem.flood import stations_level_over_threshold
from random_data import random_stations, update_random_water_levels, test_stations

def test():
        
    
    # Build list of stations
    stations = random_stations()
    N = len(stations)
    #update water levels
    update_random_water_levels(stations)
    
    #stations over threshold
    over_thresh = stations_level_over_threshold(stations, tol=0.8)
        
    assert len(over_thresh) != N
        
    #modify stations - create corrupted data
    N = len(stations)
    for i in stations[:round(N/4)]:
        i.latest_level = None
    for k in stations[round(-N/4):]:
        k.typical_range = (k.typical_range[0],-k.typical_range[1])
    for l in stations[round(N/4):round(3*N/4)]:
        l.typical_range = (l.typical_range[1], l.typical_range[0])
    #make sure that we get no results for corrupted data
    n = 0
    for i in stations:
        if i.relative_water_level() == None:
            pass
        else:
            n += 1

    assert n<(N/2)