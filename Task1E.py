# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 20:35:08 2018

@author: Ivan
"""

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def run():
    #import stations
    stations = build_station_list()
    
    #initialise the function that returns list of rivers by number of stations
    rivers_stations = rivers_by_station_number(stations, N=9)
    
    print(rivers_stations)
    
if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
