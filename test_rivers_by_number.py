# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 20:58:03 2018

@author: Ivan
"""

import pytest

from floodsystem.geo import rivers_by_station_number
from floodsystem.stationdata import build_station_list

def test_rivers_by_stations_number():
    #import stations
    stations = build_station_list()
    
    with pytest.raises(ValueError):
        rivers_by_station_number(stations, N=-9)
    with pytest.raises(ValueError):
        rivers_by_station_number(stations, N = 'hey')
    with pytest.raises(ValueError):
        rivers_by_station_number('not a list', N = 10)
    with pytest.raises(AttributeError):
        rivers_by_station_number(['list','of','elements'], N =10)
    
    