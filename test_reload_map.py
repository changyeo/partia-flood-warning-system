# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 17:08:14 2018

@author: Ivan
"""

import pytest
from setup_plotly import run
from mapboxPlot_relative_water_levels import create_map

"""Pretends to be a test but in fact it plots new data every hour."""
def test():
    run()
    create_map()