# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 21:03:31 2018

@author: Ivan
"""
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    #get stations and water levels
    stations = build_station_list()
    update_water_levels(stations)
    
    highest_rel_lev_stat = stations_highest_rel_level(stations, len(stations)+1)
    
    for i in highest_rel_lev_stat:
        print(i.name, i.relative_water_level())
    
if __name__ == '__main__':
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    run()