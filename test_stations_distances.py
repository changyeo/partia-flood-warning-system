# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 18:27:40 2018

@author: Ivan
"""
import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius, stations_by_distance

def test_stations_within_radius():
    # Build list of stations
    stations = build_station_list()
    
    #Coords for Cam City Centre:
    ccc = (52.2053, 0.1218)
    
    #get stations within 10 km of the Cambridge city centre
    stations_in_radius = stations_within_radius(stations, ccc, 10)
    
    #list for their names only
    names_only = []
    
    #get only the name and sort the list by names
    for i in stations_in_radius:
        names_only.append(i.name)
    names_only.sort()
    
    #print sorted list
    assert names_only == ['Bin Brook', 'Cambridge Baits Bite', "Cambridge Byron's Pool",
 'Cambridge Jesus Lock', 'Comberton', 'Dernford', 'Girton',
 'Haslingfield Burnt Mill', 'Lode', 'Oakington', 'Stapleford']
    
    with pytest.raises(ValueError):
        stations_within_radius(stations, ccc, 'string')
    
def test_stations_by_distance():
     # Build list of stations
    stations = build_station_list()
    
    #Coords for Cam City Centre:
    ccc = (52.2053, 0.1218)
    
    #Sorted list of distances of stations
    stationDistAll = stations_by_distance(stations,ccc)
    
    #Creates list and populates with only (station name, town, distance)
    stationDist = []
    for i in range(len(stationDistAll)):
        stationDist.append((stationDistAll[i][0].name, stationDistAll[i][0].town, stationDistAll[i][1]))
     
    #List of closest and furthest stations    
    closestStations = stationDist[:10]    
    furthestStations = stationDist[-10:]
    
    assert closestStations == [('Cambridge Jesus Lock', 'Cambridge', 0.8402364350834995), 
                               ('Bin Brook', 'Cambridge', 2.502274086951454), 
                               ("Cambridge Byron's Pool", 'Grantchester', 4.0720438555077125), 
                               ('Cambridge Baits Bite', 'Milton', 5.115589516578674), 
                               ('Girton', 'Girton', 5.227070345811418), 
                               ('Haslingfield Burnt Mill', 'Haslingfield', 7.044388165868453), 
                               ('Oakington', 'Oakington', 7.128249171700346), 
                               ('Stapleford', 'Stapleford', 7.265694306995238), 
                               ('Comberton', 'Comberton', 7.7350743760373675), 
                               ('Dernford', 'Great Shelford', 7.993861351711722)]
    assert furthestStations == [('Boscadjack', 'Wendron', 440.0026482838576), 
                                ('Gwithian', 'Gwithian', 442.05491558132354), 
                                ('Helston County Bridge', 'Helston', 443.37824966454974), 
                                ('Loe Pool', 'Helston', 445.07184458260684), 
                                ('Relubbus', 'Relubbus', 448.64944322554413), 
                                ('St Erth', 'St Erth', 449.03415711886015), 
                                ('St Ives Consols Farm', 'St Ives', 450.0734690482922), 
                                ('Penzance Tesco', 'Penzance', 456.3857579793324), 
                                ('Penzance Alverton', 'Penzance', 458.5766422710278), 
                                ('Penberth', 'Penberth', 467.53367291629183)]
    
    
    



