# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 13:18:41 2018

@author: Ivan
"""

import datetime
from floodsystem.plot_optional import plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import find_station

"""plot water levels at a specific station"""

def run():
    # Build list of stations
    stations = build_station_list()
    
    #update water levels
    update_water_levels(stations)
    
    while True:
        # ask for name
        name = input('Enter the name of station \n')
        
        try:
            #Finds stations with greatest relative water level
            search  = find_station(stations, name)
            break
        except ValueError:
            print('Invalid name')
            again = input('Do you want to search again? y/n \n')
            if again in ['y','Y','yes','Yes']:
                pass
            else:
                break
    
    
    time = float(input('Enter the number of days \n'))
    
    #make search a list
    lookup = search[0]
    # fetch data
    dates, levels = fetch_measure_levels(lookup.measure_id, dt=datetime.timedelta(days=time))

    #plot
    plot_water_levels(lookup,dates,levels)
    
if __name__ == '__main__':
    run()
    
    