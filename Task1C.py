# -*- coding: utf-8 -*-


from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    # Build list of stations
    stations = build_station_list()
    
    #Coords for Cam City Centre:
    ccc = (52.2053, 0.1218)
    
    #get stations within 10 km of the Cambridge city centre
    stations_in_radius = stations_within_radius(stations, ccc, 10)
    
    #list for their names only
    names_only = []
    
    #get only the name and sort the list by names
    for i in stations_in_radius:
        names_only.append(i.name)
    names_only.sort()
    
    #print sorted list
    print(names_only)

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()