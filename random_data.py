# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 17:26:20 2018

@author: Ivan
"""

from floodsystem.station import MonitoringStation
import random
import string


def random_stations():
#builds a random list of stations    
    stations = []
    
    #create 100 random sample stations
    for i in range(100):
        station_id = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        measure_id =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        label =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(6)])
        coord1 = 5*random.random() + 50
        coord2 = 5*random.random() - 2.5
        coord = (round(coord1,5),round(coord2,5))
        rangemin = random.random() 
        rangemax = 2*random.random()
        typical_range = (round(rangemin,3),round(rangemax,3))
        river =  ''.join([random.choice(string.ascii_letters) for n in range(5)])
        town = ''.join([random.choice(string.ascii_letters) for n in range(5)])
        s = MonitoringStation(station_id, measure_id ,label,coord,typical_range,river,town)
        stations.append(s)

    return stations

def test_stations():
    #builds a random list of stations    
    stations = []
    #create 100 random sample stations
    for i in range(4):
        station_id = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        measure_id =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        label =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(6)])
        coord1 = 5*random.random() + 50
        coord2 = 5*random.random() - 2.5
        coord = (round(coord1,5),round(coord2,5))
        rangemin = random.random() 
        rangemax = 2*random.random()
        typical_range = (round(rangemin,3),round(rangemax,3))
        river =  ''.join([random.choice(string.ascii_letters) for n in range(5)])
        town = ''.join([random.choice(string.ascii_letters) for n in range(5)])
        s = MonitoringStation(station_id, measure_id ,label,coord,typical_range,river,town)
        stations.append(s)

    stations[0].name = 'severe_station'
    stations[1].name = 'high_station'
    stations[2].name = 'moderate_station'
    stations[3].name = 'low_station'
    
    for k in stations:
        k.typical_range = (0.001,1.001)
  
    
    stations[0].latest_level = 1.6
    stations[1].latest_level = 1.3
    stations[2].latest_level = 1.0
    stations[3].latest_level = 0.6
    
    return stations
        
        
def update_random_water_levels(stations):
# attaches a random water level to each station
    for i in stations:
        i.latest_level = 3*random.random()
    