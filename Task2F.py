# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 23:25:43 2018

@author: Chang & Ivan
"""
import datetime
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    # Build list of stations
    stations = build_station_list()
    
    #update water levels
    update_water_levels(stations)
    
    #Finds stations with greatest relative water level
    relLevels = stations_level_over_threshold(stations,tol=0)
    topRelLevels = relLevels[:5]
    
    #Plots data for past 2 days for the top 5 rel levels including 
    #polynomial fit up to degree 4
    for i in topRelLevels:
        #Gets data for past 2 days
        
        dates, levels = fetch_measure_levels(i[0].measure_id, dt=datetime.timedelta(days=2))
        plot_water_level_with_fit(i[0],dates,levels,4)


if __name__ == "__main__":
    print("*** Task 2D: CUED Part IA Flood Warning System ***")
    run()
