# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 23:25:43 2018

@author: Ivan
"""
import datetime
from floodsystem.plot_optional import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_level_over_threshold
from floodsystem.analysis import assess_risk

def run():
    # Build list of stations
    stations = build_station_list()
    
    #update water levels
    update_water_levels(stations)
    
    #dictionary of risk
    risk = dict(
            severe = [],
            high = [],
            moderate = [],
            low = [],
            rising = [],
            falling = []
            )
    
    #divides stations into groups and avoid having one station in more than 1 group
    risk['severe']  = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations,tol = 1.5)]
    risk['high'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 1.2)
                        if (i[0].name, i[1]) not in risk['severe']]
    risk['moderate'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 0.9)
                        if (i[0].name, i[1]) not in risk['severe'] and (i[0].name, i[1]) not in risk['high']]
    risk['low'] = [(i[0].name, i[1]) for i in stations_level_over_threshold(stations, tol = 0.5)
                        if (i[0].name, i[1]) not in risk['severe'] and (i[0].name, i[1]) not in risk['high']and (i[0].name, i[1]) not in risk['moderate']]
    
    
    
    
    #gets stations with relative water level more than 0.5 to perform further analysis
    stations_for_analysis = stations_level_over_threshold(stations, tol = 0.5)
  
    #checks to see if low degree polynomial is increasing or decreasing (overall effect, so less effect of periodic level changes)
    for i in stations_for_analysis:
       
        dates, levels = fetch_measure_levels(i[0].measure_id, dt=datetime.timedelta(days=2))
        status = assess_risk(dates, levels)
        if status == True:
            risk['rising'].append((i[0].name, i[1]))
        elif status == False:
            risk['falling'].append((i[0].name, i[1]))
         
            
    print(risk)    
        
#    #create empty lists
#    stations = []
#    list_of_dates = []
#    list_of_levels = []
#    #Plots data for past 2 days for the top 5 rel levels including 
#    #polynomial fit up to degree 4
#    for i in topRelLevels:
#        #Gets data for past 2 days
#        dates, levels = fetch_measure_levels(i.measure_id, dt=datetime.timedelta(days=3))
#        
#        #append data to lists
#        stations.append(i)
#        list_of_dates.append(dates)
#        list_of_levels.append(levels)
#        
#        
#    plot_water_level_with_fit(stations,list_of_dates,list_of_levels,1)
        

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
