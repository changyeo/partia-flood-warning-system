# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 11:24:07 2018

@author: Ivan
"""
import plotly as ptly
import plotly.plotly as py
from plotly.graph_objs import *
from get_data_for_map import get_data
from datetime import datetime

"""Creates plot using mapbox"""
def create_map():
    mapbox_access_token = 'pk.eyJ1IjoiaXZhbi1nLTM0OCIsImEiOiJjamRpcDJlNmUwZzZkMnFvMWdvZG5jcG4yIn0.wX2OiaCe_xhLK0AvRKNZ-Q'
    
    #get data
    dataset = get_data(tol = 0.9)
    names = dataset['Name']
    typical = dataset['Typical-level']
    latest = dataset['Latest-level']
    
    #get a hover text for each station
    text = []
    
    for name, water, typicalit, latestit in zip(names, dataset['Rel-wat-lev'],typical,latest):
        text.append(str(name + '<br>' + 'Relative water level: ' + str(round(water,2))
                    + '<br>' + 'Latest water level: ' + str(round(latestit,2)) + ' m' 
                    + '<br>' + 'Typical water level: ' + str(typicalit) + ' m'))
    #size of points depends on relative water level    
    sizes = dataset['Rel-wat-lev']*15
    
    date = str(datetime.now())
    
    title = 'Relative water levels' + '-- last updated: ' + date 
    #plot data as scatterplot
    data = Data([
        Scattermapbox(
            lat=dataset['Lat'],
            lon=dataset['Lon'],
            mode='markers',
            marker=dict(
            size=sizes,
            color = dataset['Rel-wat-lev'], #set color equal to a variable
            colorscale='Viridis',
            showscale=True
        ),
            text=text,
        )
    ])
    #set up map layout        
    layout = Layout(
            title = title,
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            accesstoken=mapbox_access_token,
            bearing=0,
            center=dict(
                lat=53.5,
                lon=-4
            ),
            pitch=0,
            zoom=5, 
            style = 'mapbox://styles/ivan-g-348/cjdixq7k8191i2sqjjnbdbcpj'
        ),
    )
    
    fig = dict(data=data, layout=layout)
    py.plot(fig, filename='Relative water level at stations')
    
if __name__ == "__main__":
    create_map()
