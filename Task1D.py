# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 18:26:00 2018

@author: Chang
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river


def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()
    
    #List of all rivers
    riverStation = rivers_with_station(stations)
    
    #Length of list = number of rivers with at least one station
    print(len(riverStation))
    
    #Create list for rivers in alphabetical order
    alpRivers = []
    
    #Add rivers to list in alp order
    for key in sorted(riverStation):
        alpRivers.append(key)
        
    #Print the first 10
    print (alpRivers[:10])
    
    
    
    
    stationDict = stations_by_river(stations)
    for key in stationDict:
        if key in ('River Aire', 'River Cam', 'Thames'):
            print(sorted(stationDict[key])) 
            print('')



if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
