# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 15:24:07 2018

@author: Chang
"""

import plotly.plotly as py
from plotly.graph_objs import *
from floodsystem.stationdata import build_station_list

mapbox_access_token = 'pk.eyJ1IjoiY2hhbmd5ZW8iLCJhIjoiY2pjb3Vwd2VhMmFpeTMzcm5xcTBsbTZhMyJ9.lasYq3he1CQdM5IOhoRYww'

stations = build_station_list()
stationLon = []
stationLat = []
stationName = []

for station in stations:
    stationLat.append(station.coord[0])
    stationLon.append(station.coord[1])
    stationName.append(station.name)

data = Data([
    Scattermapbox(
        lat=stationLat,
        lon=stationLon,
        mode='markers',
        marker=Marker(
            size=9
        ),
        text=stationName,
    )
])
layout = Layout(
    autosize=True,
    hovermode='closest',
    mapbox=dict(
        accesstoken=mapbox_access_token,
        bearing=0,
        center=dict(
            lat=52.469733,
            lon=-1.836914
        ),
        pitch=0,
        zoom=5
    ),
)

fig = dict(data=data, layout=layout)
py.iplot(fig, filename='Locations of water level stations')