from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    # Build list of stations
    stations = build_station_list()
   
    #update water levels
    update_water_levels(stations)
    
 
    #stations over threshold
    over_thresh = stations_level_over_threshold(stations, tol=5)
   
    
    #print name and ratio for each station
    for i in over_thresh:
        print(i[0].name, i[1])
        

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    run()
