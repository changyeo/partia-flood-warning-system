# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 13:50:16 2018

@author: Ivan
"""

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.flood import stations_level_over_threshold

import pytz


import numpy as np

"""This function takes a list of stations and 
extracts the information that are necessary to make a graph"""
def get_data(stations):

    
    names = []
    coords = []
    lat = []
    lon = []
    rel_wat_level = []
    curr_wat_level = []
    typical_range = []
    
    
    for i in stations:
        coords.append(i.coord)
        names.append(i.name)
        lat.append(i.coord[0])
        lon.append(i.coord[1])
        rel_wat_level.append(i.relative_water_level())
        typical_range.append(i.typical_range)
        curr_wat_level.append(i.latest_level)

        
    coords = np.array(coords)
    names = np.array(names)
    lat = np.array(lat)
    lon = np.array(lon)
    rel_wat_level = np.array(rel_wat_level)
    typical_range = np.array(typical_range)
    curr_wat_level = np.array(curr_wat_level)    

    df = dict()
    df['Name'] = names
    df['Lat']= lat
    df['Lon'] = lon
    df['Rel-wat-lev'] = rel_wat_level
#    df['Percent'] = percent_of_range
    df['Latest-level'] = curr_wat_level
    df['Typical-level'] = typical_range
    df['Coords'] = coords

    
    return df


"""Build a list of stations, select those over threshold, and go back in time
by amount t to get the water level history at each station"""
def get_stations_history(t, thresh = 0.8):

     # Build list of stations
    stations = build_station_list()
   
    #update water levels
    update_water_levels(stations)
    
 
    #stations over threshold
    over_thresh = stations_level_over_threshold(stations, tol=0.8)
    
    time_history = dict()
    
    for i in over_thresh:
        try:
            dates, levels = fetch_measure_levels(i[0].measure_id, dt = datetime.timedelta(days=t))
            time_history[i[0].coord] = (dates, levels)
        except KeyError:
            pass
    
    stations_output = [i[0] for i in over_thresh]
    return stations_output, time_history

"""Take time history of water levels at a station and extracts the value
from t days ago"""
def get_level_in_history(time_history, t):

    utc=pytz.UTC
    time_back = datetime.timedelta(days = t)
    
    start_date = datetime.datetime.now() - time_back
    start_date = utc.localize(start_date) 
    
    dates, levels = time_history[0], time_history[1]
    k = 0
    while True:
        try:
            if dates[k] > start_date:
                k += 1
            else:
                required_level = levels[k]
                break
        except IndexError:
            required_level = None
            break
    return required_level
    
def update_stations(station, required_level):
    """Modify the relative water level by certain value"""
    station.latest_level = required_level
    return station


def ges(stations, history, days_back, t = 2):
    """Takes a list of stations and modifies the latest level of each by the value
    from t days ago."""

    for i in stations:
        try:
            level_in_time = get_level_in_history(history[i.coord], t = days_back)
            i.latest_level = level_in_time
        except KeyError:
            i.latest_level = None
    
    return stations