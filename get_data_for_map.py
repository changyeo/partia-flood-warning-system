# -*- coding: utf-8 -*-
"""
Created on Sun Jan 21 13:50:16 2018

@author: Ivan
"""

from floodsystem.stationdata import build_station_list, update_water_levels

from floodsystem.flood import stations_level_over_threshold

import numpy as np

"""Provides the necessary data to plot stations on a map."""
def get_data(tol = 0.9):
    
    # Build list of stations
    stations = build_station_list()
   
    #update water levels
    update_water_levels(stations)
    
 
    #stations over threshold
    over_thresh = stations_level_over_threshold(stations, tol)
    
    names = []
    lat = []
    lon = []
    rel_wat_level = []
    curr_wat_level = []
    typical_range = []
    
    for i in over_thresh:
        names.append(i[0].name)
        lat.append(i[0].coord[0])
        lon.append(i[0].coord[1])
        rel_wat_level.append(i[1])
        typical_range.append(i[0].typical_range)
        curr_wat_level.append(i[0].latest_level)
    
    names = np.array(names)
    lat = np.array(lat)
    lon = np.array(lon)
    rel_wat_level = np.array(rel_wat_level)
    typical_range = np.array(typical_range)
    current_wat_level = np.array(curr_wat_level)    
    
        
    df = dict()
    df['Name'] = names
    df['Lat']= lat
    df['Lon'] = lon
    df['Rel-wat-lev'] = rel_wat_level

    df['Latest-level'] = current_wat_level
    df['Typical-level'] = typical_range
    

    
    return df

#if __name__ == "__main__":
#    get_data()
#    
