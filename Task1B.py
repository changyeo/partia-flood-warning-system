# -*- coding: utf-8 -*-
"""
Created on Thu Jan 18 20:54:42 2018

@author: Chang
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    # Build list of stations
    stations = build_station_list()
    
    #Coords for Cam City Centre:
    ccc = (52.2053, 0.1218)
    
    #Sorted list of distances of stations
    stationDistAll = stations_by_distance(stations,ccc)
    
    #Creates list and populates with only (station name, town, distance)
    stationDist = []
    for i in range(len(stationDistAll)):
        stationDist.append((stationDistAll[i][0].name, stationDistAll[i][0].town, stationDistAll[i][1]))
     
    #List of closest and furthest stations    
    closestStations = stationDist[:10]    
    furthestStations = stationDist[-10:]
    
    print(closestStations)
    print(furthestStations)
    

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()