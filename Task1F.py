# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 21:16:41 2018

@author: Chang
"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations


def run():
    """Requirements for Task 1A"""

    # Build list of stations
    stations = build_station_list()

    inconsistentStationsObj = inconsistent_typical_range_stations(stations)
    
    #List of inconsistent station names to print in sorted order
    inconsistentStations = []
    
    for station in inconsistentStationsObj:
        inconsistentStations.append(station.name)
        
    print(sorted(inconsistentStations))


if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")
    run()
