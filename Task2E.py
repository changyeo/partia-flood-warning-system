# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 22:15:43 2018

@author: Chang
"""
import datetime
from floodsystem.plot import plot_water_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    # Build list of stations
    stations = build_station_list()
    
    #update water levels
    update_water_levels(stations)
    
    #Finds stations with greatest relative water level
    relLevels = stations_highest_rel_level(stations,5)
    #topRelLevels = relLevels[:5]
    
    
    #Lists for stations, dates, levels
    stationList = []
    dateList = []
    levelList = []
    
    #Plots data for past 10 days for the top 5 rel levels
    for i in relLevels:
        #Gets data for past 10 days
        dates, levels = fetch_measure_levels(i.measure_id, dt=datetime.timedelta(days=10))
        stationList.append(i)
        dateList.append(dates)
        levelList.append(levels)
        
    plot_water_levels(stationList,dateList,levelList)


if __name__ == "__main__":
    print("*** Task 2D: CUED Part IA Flood Warning System ***")
    run()
