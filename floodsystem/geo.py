"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key
#from math import sin, cos, sqrt, atan2, radians
from haversine import haversine
#"""
#Check if you use pip or pip3 notation
#"""
#try:
#    import pip3
#    installed_packages = pip3.get_installed_distributions()
#except ModuleNotFoundError:
#    import pip
#    installed_packages = pip.get_installed_distributions()
#installed_packages_list = [i.key for i in installed_packages]
#
#"""
#If haversine is installed on a computer, use that version. If it isn't, 
#then use the files in repository.
#"""
#if 'haversine' in installed_packages_list:
#    from haversine import haversine
#else:
#    from haversine.haversine import haversine


def stations_by_distance(stations, p):
    stationDist = []
    
    #For every station in list of stations get coordinates
    for station in stations:
        coords = station.coord
        
        """ Manual Calc
        #Splitting into lat and lon
        lat1 = radians(coords[0])
        lon1 = radians(coords[1])
        lat2 = radians(p[0])
        lon2 = radians(p[1])
        
        #Calc differences 
        dlat = lat2 - lat1
        dlon = lon2 - lon1
        
        # Approx radius of Earth in km
        rad = 6373.0
        
        #Haversine formula for distances between coordinates
        a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        
        dist = rad * c
        """
                   
        dist = haversine(coords,p)
        
        #Appends to list
        stationDist.append((station,dist))
    
    #Sorts list by distance
    sortedList = sorted_by_key(stationDist,1)
    return sortedList

        
def stations_within_radius(stations, centre, r):
# returns a list of stations within a specified radius from a point
    if (type(r) != float and type(r) != int):
        raise ValueError('Radius has to be a number')
    stationsDist = stations_by_distance(stations, centre)
    #import list of tuples (station, distance from centre) by calling function stations_by_distance
    
    stations_within = []
    #create empty list
    
    #iterate over stations and compare distance to radius
    for i in stationsDist:
        if i[1]<=r:
            stations_within.append(i[0])
            #append those within radius
        else:
            break
            #since stationsDist is sorted by distance, we don't have to go over the whole list
    
    #returns list of stations that is sorted by distance from centre
    return stations_within
            
           

def rivers_with_station(stations):
    #Creates an empty set to hold river names
    riverSet = set()
    
    #Adds river name of station into set; all duplicates ignored as set values must be unique
    for station in stations:
        riverSet.add(station.river)
    
    return riverSet
        


def stations_by_river(stations):
    #Creates empty dictionary
    riverDict = {}
    
    #For a station:
    for station in stations:
        #If station's river is already in dictionary, append station name 
        #to the cooresponding list
        if station.river in riverDict:
            riverDict[station.river].append(station.name)
        
        #Or if not, create a new dictionary value with river name as the key 
        #and station name as value (in list form)
        else:
            riverDict[station.river] = [station.name]
        
    return riverDict
                    

def rivers_by_station_number(stations,N):
    #check input
    if type(N)!=int or N<0:
        raise ValueError('Number of rivers has to be a positive integer')
    if type(stations) != list:
        raise ValueError('Input of function has to be a list of classes MonitoringStation')
    #import dictionary of stations by rivers
    riverDict = stations_by_river(stations)        
    #create empty list
    river_number_stations = []
    #iterate over dictionary of rivers and stations and return list of tuples
    #with the number of stations on each river
    for key in riverDict:
        river_number_stations.append((key, len(riverDict[key])))
    #sort the list by number of rivers    
    sortedList = sorted_by_key(river_number_stations,1, reverse = True)
    
    #create empty list
    n_largest_values = []
    #initial value
    i = 0
    #continue adding tuples of rivers to the list until the next river has
    #more stations than the current one
    while True:
        if i<N:
            pass
        else:
            if sortedList[i][1] == sortedList[i-1][1]:
                pass
            else:
                break
        n_largest_values.append(sortedList[i])
        i += 1
        
            
    #return the list of N tuples (river,number of stations)        
    return n_largest_values
        