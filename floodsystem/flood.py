# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 16:52:01 2018

@author: Ivan
"""


def stations_level_over_threshold(stations,tol):
#returns a list of tuples 
    
        
    #create a list of consistent stations
    consistent_stations = [i for i in stations if i.typical_range_consistent()==True]
    
    #creates a list of stations over threshold
    over_thresh_stations = [(i, i.relative_water_level()) for 
                            i in consistent_stations if(i.relative_water_level()!= None 
                            and i.relative_water_level() > tol )]
    #sort list
    over_thresh_stations.sort(reverse = True)
    
    return over_thresh_stations

def stations_highest_rel_level(stations, N):
#returns a list of stations with highest relative water level
    
    #create a list of consistent stations with data
    consistent_stations = [i for i in stations if (
            i.typical_range_consistent()==True and i.relative_water_level() != None)]
    #sort the list
    consistent_stations.sort(reverse = True)
    #return first N entries
    return consistent_stations[:(N)]

def find_station(stations, name):
    """retrieves the station by its name"""    
    # iterates over list of stations
    result = [i for i in stations if i.name == name]
    if len(result) == 0:
        raise ValueError('no such station')
    
    return result