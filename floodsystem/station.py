"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None


    def __repr__(self):
        d =  "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += "   measure id:    {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d


    def typical_range_consistent(self):
        #Only returns true if typical_range has data and high > low
        typRange = self.typical_range
        
        #Checks that typical_range is a tuple and not NoneType
        if type(typRange) is tuple:
            #Vars for low and high range
            if type(typRange[0]) is float and type(typRange[1]) is float:
                low = self.typical_range[0]
                high = self.typical_range[1]
                
                #Checks if high is bigger (or equal) than low
                if high >= low:
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    
    
    def relative_water_level(self):
    #return the relative water level
    # check for consistency of data and 
        try:
            if (self.typical_range_consistent() == True and 
                isinstance(self.latest_level, float)) and self.latest_level > 0:
                ratio = (self.latest_level - self.typical_range[0])/(self.typical_range[1]-
                        self.typical_range[0])
                return round(ratio, 3)
            else:
                return None
        except:
            return None

    def __lt__(self,other):
    #provides the ability to directly sort stations by water level
        if self.relative_water_level() < other.relative_water_level():
            return True
        else:
            return False
        
        
def inconsistent_typical_range_stations(stations):
    #Create list for inconsistent stations:
    inconsistents = []
    
    #Iterate through stations and add to list if inconsistent
    for station in stations:
        rangeResult = MonitoringStation.typical_range_consistent(station)
        
        if rangeResult == False:
            inconsistents.append(station)
        
    return inconsistents
        
        
        
        
        
        
        
        
        
        
        
        
        
        