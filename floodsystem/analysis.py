# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 22:54:05 2018

@author: Ivan
"""

import numpy as np
import matplotlib


def polyfit(dates,levels,p):
    #change dates into floats
    x = matplotlib.dates.date2num(dates)
    y = levels
    #polynomial fit
    fit = np.polyfit(x-x[0],y,p)
    #polynomial function
    poly = np.poly1d(fit)
    
    return (poly, x[0])
 
    
def assess_risk(dates, levels):
    poly, x0 = polyfit(dates, levels, 1)
    x = matplotlib.dates.date2num(dates)
    y = levels
    deriv = np.polyder(poly)
    change = deriv(x[-1])
    if change > 0:
        increasing = True
    else:
        increasing = False
    return increasing