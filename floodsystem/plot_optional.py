# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 21:51:32 2018

@author: Chang
"""

import matplotlib.pyplot as plt
from datetime import datetime, timedelta

from .analysis import polyfit
from matplotlib.dates import date2num

def plot_water_levels(station, dates, levels):
    
    
    # Plot
    plt.plot(dates, levels)
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    
    #Data points for typical low and high
    length = len(dates)
    low = [station.typical_range[0]] * length
    high = [station.typical_range[1]] * length
    
    #Plots typical low and high
    plt.plot(dates,low)
    plt.plot(dates,high)
    
    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
    
    
def plot_water_level_with_fit(stations, dates, levels, p):
#plots water levels and also polynomial
    N = len(stations)
   
    fig = plt.figure(figsize = (6,6))
    
    if N < 3:
        nrows = 1
    elif N > 2 and N < 5:
        nrows = 2
    elif N > 4 and N < 7:
        nrows = 3
    else:
        raise RuntimeError('Maximum number of stations is 6')
    
    for i in range(N):
        
        #perform a polynomial fit and get data
        poly, d0 = polyfit(dates[i], levels[i], p)
        
        #shift dates
        x = date2num(dates[i])
        xshift = x - d0
        
        #get y-axis data from fit
        y = poly(xshift)
        #create figure and axes
        fig.add_subplot(nrows,2,(i+1))
        
        ax = fig.gca()
        #plot data
        ax.plot(dates[i], levels[i], 'r.', markersize = 2, label = 'Water level')
        ax.plot(dates[i], y)
        ax.set_title(stations[i].name, fontsize= 7)
        ax.set_xlabel('Date')
        ax.set_ylabel('Water level (m)', fontsize = 7)
        fig.autofmt_xdate()
        
        #Data points for typical low and high
        length = len(dates[i])
        low = [stations[i].typical_range[0]] * length
        high = [stations[i].typical_range[1]] * length
        #plot typical high and low
        ax.plot(dates[i], low, linewidth = 0.5, linestyle = '--', color = 'black', label = 'Typical low level')
        ax.plot(dates[i], high, linewidth = 0.5, color = 'black', label = 'Typical high level')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
       
    #plt.xlabel('Date')
    #plt.ylabel('Water level (m)') 
    plt.legend(bbox_to_anchor=(1.15, 0.3), loc='lower left', borderaxespad=0.)
    plt.tight_layout()
    plt.show()
    