Our extensions are based on the idea of plotting the water level data on a map.

The first part is an online plot that is available at 

https://plot.ly/~ivan-g-348/5/
 or 
https://plot.ly/~ivan-g-348/5.embed

It uses an online plotting service Plotly combined with mapping library mapbox.
The source code of this plot is in the repository, with the main file being mapboxPlot_relative_water_levels.py
This program calls a function get_data from file get_data_for_map.py with an argument which is the threshold of relative water level of plotted stations. 
Then an online plot is produced that can be accessed at either of the two links.
The system is automated. There is a "test" called test_reload_map.py which is a program that creates a plot with new data. The pipelines on bitbucket are configured to run every hour, so that the plot updates at regular intervals.

The second part of our extension is a Jupyter notebook Plot_history_of_data.ipynb
The modules necessary to run this notebook are datetime, pytz, numpy and plotly.
The purpose of this extension is to be able to go back in time and see how the water levels at stations developed. There are two independent cells, one is a regular slider, and the other one is a slider with manual reload. Moving over large time steps is quite demanding on memory, so it is better to use manual reload in that case. 
