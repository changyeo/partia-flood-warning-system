import plotly.plotly as py
import pandas as pd
from plotly.tools import set_credentials_file
from floodsystem.stationdata import build_station_list

set_credentials_file(username='Changyeo', api_key='yMyZc87AnRvPGar4z4tR')

#df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/2011_february_us_airport_traffic.csv')
#df.head()

#df['text'] = df['airport'] + '' + df['city'] + ', ' + df['state'] + '' + 'Arrivals: ' + df['cnt'].astype(str)

stations = build_station_list()
stationLon = []
stationLat = []
stationName = []

for station in stations:
    stationLat.append(station.coord[0])
    stationLon.append(station.coord[1])
    stationName.append(station.name)



scl = [ [0,"rgb(5, 10, 172)"],[0.35,"rgb(40, 60, 190)"],[0.5,"rgb(70, 100, 245)"],\
    [0.6,"rgb(90, 120, 245)"],[0.7,"rgb(106, 137, 247)"],[1,"rgb(220, 220, 220)"] ]

data = [ dict(
        type = 'scattergeo',
        locationmode = 'UK',
        lon = stationLon,
        lat = stationLat,
        text = stationName,
        mode = 'markers',
        marker = dict(
            size = 4,
            opacity = 0.8,
            reversescale = True,
            autocolorscale = False,
            symbol = 'circle',
            line = dict(
                width=1,
                color='rgba(102, 102, 102)'
            ),
            colorscale = scl,
            cmin = 0,
            )
        )]

layout = dict(
        title = 'Locations of water level stations',
        geo = dict(
            scope='europe',
            projection=dict( type='Equirectangular' ),
            showland = True,
            landcolor = "rgb(250, 250, 250)",
            center = (52.603214,-0.973389),
            subunitcolor = "rgb(217, 217, 217)",
            countrycolor = "rgb(217, 217, 217)",
            countrywidth = 0.5,
            subunitwidth = 0.5
        ),
    )

fig = dict( data=data, layout=layout )
py.iplot( fig, validate=False, filename='d3-airports' )