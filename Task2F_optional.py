# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 23:25:43 2018

@author: Ivan
"""
import datetime
from floodsystem.plot_optional import plot_water_level_with_fit
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    # Build list of stations
    stations = build_station_list()
    
    #update water levels
    update_water_levels(stations)
    
    #Finds stations with greatest relative water level
    topRelLevels  = stations_highest_rel_level(stations,5)
        
    #create empty lists
    stations = []
    list_of_dates = []
    list_of_levels = []
    #Plots data for past 2 days for the top 5 rel levels including 
    #polynomial fit up to degree 4
    for i in topRelLevels:
        #Gets data for past 2 days
        dates, levels = fetch_measure_levels(i.measure_id, dt=datetime.timedelta(days=3))
        
        #append data to lists
        stations.append(i)
        list_of_dates.append(dates)
        list_of_levels.append(levels)
        
        
    plot_water_level_with_fit(stations,list_of_dates,list_of_levels,1)
        

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
